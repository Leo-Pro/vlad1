<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Avatar extends Model
{
    protected $fillable = [
        'id',
        'userid',
        'file',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];
}
