<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $table = 'news';

    protected $fillable = [
        'id',
        'photo',
        'name',
        'description',
        'content',
        'publish',
        'published_at',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];
}
