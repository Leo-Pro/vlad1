<?php

namespace App\Http\Controllers;

use App\Models\News;
use App\Http\Requests\StoreNewsRequest;
use App\Http\Requests\UpdateNewsRequest;
use App\Http\Resources\NewsResource;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use Carbon\Carbon;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $index = new NewsResource(News::query());

        $newss = News::query();
        $dataFalse = $newss->where('publish', false);

        for ($i = 0; $i < $newss->where('publish', false)->count(); $i++) {
            if ( $dataFalse->get('published_at')[$i]->published_at >= Carbon::now()->format('Y-m-d H:i:s')) {
                $newss
                    ->where('published_at', $dataFalse->get('published_at')[$i]->published_at)
                    ->update(['publish' => true]);
            }
        }

        $offset = $request->input('offset', 0);
        $limit = $request->input('limit', 10);
        $total = $index->where('publish', true)->count();

        $page_data = $index->where('publish', true)->offset($offset)->limit($limit)->get();
        $result = [$page_data, 'total' => $total];
        return response()->json($result, Response::HTTP_OK);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreNewsRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreNewsRequest $request)
    {
        $result = new News();
        $result->name = $request->input('name');
        if ($request->hasFile('photo')) {
            $result->photo = $request->file('photo')->store('news', 'public');
        }
        $result->description = $request->input('description');
        $result->content = $request->input('content');
        $result->publish = $request->boolean('publish');
        $result->published_at = Carbon::make($request->input('published_at'))->format('d-m-Y H:i:s');
        $result->save();
        return response()->json($result, Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\News  $news
     * @return \Illuminate\Http\Response
     */
    public function show(News $news)
    {
        $show = new NewsResource($news);
        return response()->json($show, Response::HTTP_OK);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateNewsRequest  $request
     * @param  \App\Models\News  $news
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateNewsRequest $request, News $news)
    {
        if ($request->hasFile('photo')) {
            $news->photo = $request->file('photo')->store('news', 'public');
        }
        $news->name = $request->input('name');
        $news->description = $request->input('description');
        $news->content = $request->input('content');
        $news->publish = $request->input('publish');
        $news->published_at = Carbon::make($request->input('published_at'))->format('d-m-Y H:i:s');
        $news->save();
        return response()->json($news, Response::HTTP_ACCEPTED);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\News  $news
     * @return \Illuminate\Http\Response
     */
    public function destroy(News $news)
    {
        $news->delete();
        return response()->json(null, Response::HTTP_NO_CONTENT);
    }
}
