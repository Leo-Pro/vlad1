<?php

namespace App\Http\Controllers\users;

use App\Http\Controllers\Controller;
use App\Models\Avatar;
use App\Http\Requests\StoreAvatarRequest;
use App\Http\Requests\UpdateAvatarRequest;
use App\Http\Resources\AvatarResource;
use Illuminate\Http\Response;



class AvatarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $result = AvatarResource::collection(Avatar::all());
        return response()->json($result, Response::HTTP_OK);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreAvatarRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreAvatarRequest $request)
    {
        $store = new Avatar();

        $store->userid = $request->input('userid');

        if ($request->hasFile('file')) {
            $store->file = $request->file('file')->store('users/avatars', 'public');
        }
        $store->save();
        $result = new AvatarResource($store);

        return response()->json($result, Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Avatar  $avatar
     * @return \Illuminate\Http\Response
     */
    public function show(Avatar $avatar)
    {
        $result = new AvatarResource($avatar);
        return response()->json($result, Response::HTTP_OK);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateAvatarRequest  $request
     * @param  \App\Models\Avatar  $avatar
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateAvatarRequest $request, Avatar $avatar)
    {
        $avatar->userid = $request->userid;
        if ($request->hasFile('file')) {
            $avatar->file('file')->store('users/avatars', 'public');
        }
        return response()->json($avatar, Response::HTTP_ACCEPTED);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Avatar  $avatar
     * @return \Illuminate\Http\Response
     */
    public function destroy(Avatar $avatar)
    {
        $avatar->delete();
        return response()->json(null, Response::HTTP_NO_CONTENT);
    }
}
