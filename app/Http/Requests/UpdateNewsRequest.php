<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateNewsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'photo' => ['required', 'image'],
            'name' => ['required', 'max:255'],
            'description' => ['required', 'max:5000'],
            'content' => ['nullable', 'max:15000'],
            'publish' => ['boolean'],
            'published_at' => ['nullable', 'date']
        ];
    }
}
